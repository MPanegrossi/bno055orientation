#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager

#include <OSCMessage.h>
#include <OSCBundle.h>
#include <OSCData.h>

IPAddress MyIp;
//IPAddress remoteIP;

int ip = 6;

IPAddress remoteIp;
IPAddress outIp(192, 168, 0, ip);     // remote IP of your computer


//SETUP UDP
WiFiUDP Udp;

//LISTENING ON PORT
unsigned int localPort = 9000;

//SENDING ON PORT
unsigned int outPort = 9001;

OSCErrorCode error;

/* This driver reads raw data from the BNO055

   Connections
   ===========
   Connect SCL to analog 5
   Connect SDA to analog 4
   Connect VDD to 3.3V DC
   Connect GROUND to common ground

   History
   =======
   2015/MAR/03  - First release (KTOWN)
*/

/* Set the delay between fresh samples */
#define BNO055_SAMPLERATE_DELAY_MS (20)

Adafruit_BNO055 bno = Adafruit_BNO055();

/**************************************************************************/
/*
    Arduino setup function (automatically called at startup)
*/
/**************************************************************************/
void setup(void)
{
  Serial.begin(115200);

  pinMode(16, OUTPUT);

  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  //reset saved settings
  //wifiManager.resetSettings();

  //set custom ip for portal
  wifiManager.setSTAStaticIPConfig(IPAddress(192, 168, 0, 100), IPAddress(192, 168, 0, 1), IPAddress(255, 255, 255, 0));

  //fetches ssid and pass from eeprom and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  wifiManager.autoConnect("AutoConnectAP");
  //or use this for auto generated name ESP + ChipID
  //wifiManager.autoConnect();


  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");

  //START THE UDP CONNECTION
  if (Udp.begin(localPort))
  {
    //Serial.println("Listening on port: ");
    //Serial.println(localPort);
  }

  MyIp = WiFi.localIP();
  //Serial.print("My IP: ");
  //Serial.println(MyIp);

  digitalWrite(16, LOW);


  //Serial.println("Orientation Sensor Raw Data Test"); Serial.println("");

  /* Initialise the sensor */
  if (!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    //Serial.print("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    while (1);
  }

  delay(1000);

  /* Display the current temperature */
  /* int8_t temp = bno.getTemp();
    Serial.print("Current Temperature: ");
    Serial.print(temp);
    Serial.println(" C");
    Serial.println("");*/

  bno.setExtCrystalUse(true);

  //Serial.println("Calibration status values: 0=uncalibrated, 3=fully calibrated");
}

/**************************************************************************/
/*
    Arduino loop function, called once 'setup' is complete (your own code
    should go here)
*/
/**************************************************************************/
void loop(void)
{
  // Possible vector values can be:
  // - VECTOR_ACCELEROMETER - m/s^2
  // - VECTOR_MAGNETOMETER  - uT
  // - VECTOR_GYROSCOPE     - rad/s
  // - VECTOR_EULER         - degrees
  // - VECTOR_LINEARACCEL   - m/s^2
  // - VECTOR_GRAVITY       - m/s^2

  sendQuaternion();
  /* Display calibration status for each sensor. */
  uint8_t system, gyro, accel, mag = 0;
  bno.getCalibration(&system, &gyro, &accel, &mag);

  delay(BNO055_SAMPLERATE_DELAY_MS);

  //CONTINUOSLY CHECK FOR INCOMING PACKETS
  int packetSize = Udp.parsePacket();

  //IF Packet is received
  if (packetSize > 0) {

    //CREATE MESSAGE TO FILL WITH INCOMING DATA
    OSCMessage msg;

    remoteIp = Udp.remoteIP();

    //Fill the empty OSC message with the incoming message
    while (packetSize--) {
      msg.fill(Udp.read());
    }

    //Route the message
    if (!msg.hasError()) {
      msg.dispatch("/ip", ipChange);


    } else {
      error = msg.getError();
      Serial.print("error: ");
      Serial.println(error);
    }

  }

}

//_-----------OSC FUNCTIONS

void ipChange(OSCMessage &msg) {

  //DO SOMETHING HERE
  ip = msg.getInt(0);
  IPAddress newIp(192, 168, 1, ip);
  outIp = newIp;

}


void sendQuaternion() {
  // Quaternion data
  imu::Quaternion quat = bno.getQuat();
  float quatW = quat.w();
  float quatY = quat.y();
  float quatX = quat.x();
  float quatZ = quat.z();

  OSCBundle msg;
  msg.add("/quadernone").add(quatW).add(quatY).add(quatX).add(quatZ);
  Udp.beginPacket(outIp, outPort);
  msg.send(Udp);
  Udp.endPacket();
  msg.empty();
}

void sendEuler() {
  //Euler Data
  imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
  float eulerX = euler.x();
  float eulerY = euler.y();
  float eulerZ = euler.z();

  OSCBundle msg;
  msg.add("/euler").add(eulerX).add(eulerY).add(eulerZ);
  Udp.beginPacket(outIp, outPort);
  msg.send(Udp);
  Udp.endPacket();
  msg.empty();
}

