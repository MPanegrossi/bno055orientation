﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BitScience;

public class TestLocationService : MonoBehaviour
{

    public Text longitude;
    public Text latitude;
    public Text altitude;
    public Text accuracy;
    public Text lastTime;
    public bool runInBackground = true;
    //public StreetViewCube streetView;



    IEnumerator Start()
    {
        // First, check if user has location service enabled
        Application.runInBackground = runInBackground;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        if (!Input.location.isEnabledByUser)
            yield break;

        // Start service before querying location
        Input.location.Start(0.1f,0.1f);

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            longitude.text = ("Timed out");
            latitude.text = ("Timed out");
            altitude.text = ("Timed out");
            accuracy.text = ("Timed out");
            lastTime.text = ("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            longitude.text = ("Unable to determine device location");
            latitude.text = ("Unable to determine device location");
            altitude.text = ("Unable to determine device location");
            accuracy.text = ("Unable to determine device location");
            lastTime.text = ("Unable to determine device location");
            yield break;
        }
        else
        {
            // Access granted and location value could be retrieved
            longitude.text = Input.location.lastData.longitude.ToString();
            latitude.text = Input.location.lastData.latitude.ToString();
            altitude.text = Input.location.lastData.altitude.ToString();
            accuracy.text = Input.location.lastData.horizontalAccuracy.ToString();
            lastTime.text = Input.location.lastData.timestamp.ToString();


            //testo.text = ("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
        }

        // Stop service if there is no need to query location updates continuously
        //Input.location.Stop();
    }

    void Update()
    {
        longitude.text = Input.location.lastData.longitude.ToString();
        latitude.text = Input.location.lastData.latitude.ToString();
        altitude.text = Input.location.lastData.altitude.ToString();
        accuracy.text = Input.location.lastData.horizontalAccuracy.ToString();
        lastTime.text = Input.location.lastData.timestamp.ToString();

        //streetView.Latitude = Input.location.lastData.latitude;
        //streetView.Longitude = Input.location.lastData.longitude;

    }
    
}