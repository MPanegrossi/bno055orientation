﻿using UnityEngine;
using System.Collections;

// Get the latest webcam shot from outside "Friday's" in Times Square
public class testRequest : MonoBehaviour
{
    public string url = "https://maps.googleapis.com/maps/api/streetview?size=512x512&location=-23.58341,-46.68157&fov=90&heading=0&key=AIzaSyA75liyohoZMaEOkiCMSbmoUQmCXVovfGM";

    IEnumerator Start()
    {
        // Start a download of the given URL
        WWW www = new WWW(url);

        // Wait for download to complete
        yield return www;

        // assign texture
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = www.texture;
    }
}