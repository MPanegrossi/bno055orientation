/*
* UniOSC
* Copyright © 2014-2015 Stefan Schlupek
* All rights reserved
* info@monoflow.org
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using OSCsharp.Data;


namespace UniOSC{

	/// <summary>
	/// Rotates (localRotation) the hosting game object.
	/// For every axis you have a separate OSC address to specify
	/// </summary>
	[AddComponentMenu("UniOSC/UniOSCRotateQuaternionGameObject")]
	public class UniOSCRotateQuaternionGameObject :  UniOSCEventTarget {

		#region public
		[HideInInspector]
		public Transform transformToRotate;

		public string X_Address;
        public float smooth;

        #endregion

        #region private
        private Vector3 eulerAngles;
		private Quaternion rootRot;
		private float cx,cy,cz,cw;
		private Quaternion rx,ry,rz,rw;
		#endregion


		void Awake(){

		}

		public override void OnEnable(){
			_Init();
			base.OnEnable();
		}

		private void _Init(){
			
			//receiveAllAddresses = false;
			_oscAddresses.Clear();
			if(!_receiveAllAddresses){
				_oscAddresses.Add(X_Address);
			}
			cx=0f;cy=0f;cz = 0f;cw=0f;
			if(transformToRotate == null){
				Transform hostTransform = GetComponent<Transform>();
				if(hostTransform != null) transformToRotate = hostTransform;
			}
			
			rootRot = transformToRotate.localRotation;
		}
	

		public override void OnOSCMessageReceived(UniOSCEventArgs args){
		
			if(transformToRotate == null) return;
            
			OscMessage msg = (OscMessage)args.Packet;

			if(msg.Data.Count <1)return;
			//if(!( msg.Data[0] is System.Double))return;

			float value0 = (float)msg.Data[3];
            float value1 = (float)msg.Data[1];
            float value2 = (float)msg.Data[0];
            float value3 = (float)msg.Data[2];
            //float value0 = (float)msg.Data[0];
            //wyxz --> xyzw


            cx = value0;
			cy = value1;
			cz = value2;
            cw = value3;

            //Quaternion rotazione = new Quaternion(cz, cw * -1, cy, cx * -1);
            Quaternion rotazione = new Quaternion(cz * -1, cw, cy * -1, cx * -1);

            transform.rotation = Quaternion.Lerp(transform.rotation, rotazione, smooth);
            //Debug.Log("0: " + cz + "| 1: " + cw + "| 0: " + cy + "| 0: " + cx);
            /*
            rx = Quaternion.AngleAxis (cx,  Vector3.right); 
			ry = Quaternion.AngleAxis (cy , Vector3.up);
			rz = Quaternion.AngleAxis (cz,  Vector3.forward);


			transformToRotate.localRotation = rootRot * rx*ry*rz;
            */

		}


	}

}